<?php session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
	}

	public function doLogin() {
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','Password','required');
		if($this->form_validation->run() !== FALSE ) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => site_url("api/login"),
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "username=$username&password=$password",
			  CURLOPT_HTTPHEADER => array(
			    "content-type: application/x-www-form-urlencoded"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			if(!$err) {
				$res = json_decode($response, true);
				if($res['status'] == 'success') {
					$_SESSION['token'] = $res['token'];
				}
				echo $response;
			} else {
				$res['status'] = "error";
				$res['message'] = "something went wrong, please try after sometime";
				json_encode($res);
			}
		} else {
			$res['status'] = false;
			$res['message'] = str_replace("\n", '', strip_tags(validation_errors()));
			json_encode($res);
		}
	}

	public function imageupload() {
		if(empty($_SESSION['token'])) {
			redirect(site_url());
		}
		$this->load->view('upload');
	}

	public function logout() {
		session_destroy();
		redirect(site_url('demo'));
	}

	public function doUpload() {
		$this->form_validation->set_rules('width','Width','required');
		$this->form_validation->set_rules('height','Height','required');

		if(empty($_FILES['image_file']['name']) && empty($_POST['image_url'])) {
			$res['status'] = false;
			$res['message'] = "Image url or image required";
			json_encode($res);
			exit;
		}

		if($this->form_validation->run() !== FALSE ) {

			if(!empty($_FILES['image_file']['name'])) {

				$config['upload_path'] = './public/uploads/';
	            $config['allowed_types'] = 'jpg|jpeg|png';
	            $config['max_size'] = '1024';
	            $config['encrypt_name'] = TRUE; 
	            $this->upload->initialize($config);
	            if ( ! $this->upload->do_upload('image_file')) {
                    echo json_encode(array('status'=>true, 'message' => $this->upload->display_errors() ));
                    exit;
                }

                $upload_data = $this->upload->data();
                $image_file_name = $upload_data['file_name'];
                $file_path = $config['upload_path'].$image_file_name;

			} else {
				$file_path = $this->input->post('image_url');
			}

			$width = $this->input->post('width');
			$height = $this->input->post('height');

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => site_url("api/image_upload"),
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => "width=$width&height=$height&image_url=$file_path",
			  CURLOPT_HTTPHEADER => array(
			  	"Authorization: ".$_SESSION['token']."",
			    "content-type: application/x-www-form-urlencoded"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			if(!$err) {
				$res = json_decode($response, true);
				
				echo $response;
			} else {
				$res['status'] = "error";
				$res['message'] = "something went wrong, please try after sometime";
				json_encode($res);
			}

		} else {
			$res['status'] = false;
			$res['message'] = str_replace("\n", '', strip_tags(validation_errors()));
			json_encode($res);
		}
	}
}
