<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class Api extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']); 
    }

	private $errorResponse = array(
		'status' => 'error',
		'message' => ''
	);

	private $successRespnose = array(
		'status' => 'success'
	);

	public function image_upload() {

		if(strtoupper($this->input->server('REQUEST_METHOD'))=='POST') {
			$this->form_validation->set_rules('width','Width','required');
			$this->form_validation->set_rules('height','Height','required');
			$this->form_validation->set_rules('image_url','Image','required');
			if($this->form_validation->run() !== FALSE ) {

				$this->verify_request();

			    $w = $this->input->post('width');
			    $h = $this->input->post('height');
			    $image_url = $this->input->post('image_url');

			    require APPPATH .'third_party/cloudinary/Cloudinary.php';
				require APPPATH .'third_party/cloudinary/Uploader.php';
				require APPPATH .'third_party/cloudinary/Api.php';
				require APPPATH .'third_party/cloudinary/Error.php';

				Cloudinary::config(array( "cloud_name" => $this->config->item('cloud_name'), "api_key" => $this->config->item('cloud_api_key'), "api_secret" => $this->config->item('cloud_api_secret') ));

				$filedata = \Cloudinary\Uploader::upload($image_url, array("width" => $w, "height" => $h, "crop" => "thumb"));
				

			    $this->successRespnose['message'] = "image uploaded";
			    $this->successRespnose['url'] = $filedata['url'];
		        $this->responseData($this->successRespnose);
		    } else {
				$this->errorResponse['message'] = str_replace("\n", '', strip_tags(validation_errors()));
				$this->responseData($this->errorResponse);
			}
		} else {
			$this->errorResponse['message'] = 'Invalid Request';
			$this->responseData($this->errorResponse);
		}
	    
	}



	public function login() {

		if(strtoupper($this->input->server('REQUEST_METHOD'))=='POST') {
			$this->form_validation->set_rules('username','username','required');
			$this->form_validation->set_rules('password','Password','required');
			if($this->form_validation->run() !== FALSE ) {
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$dummy_user = [
		            'username' => 'test',
		            'password' => 'test'
		        ];
				if ($username === $dummy_user['username'] && $password === $dummy_user['password']) {
		            // Create a token from the user data and send it as reponse
		            $token = AUTHORIZATION::generateToken(['username' => $dummy_user['username']]);
		            // Prepare the response
		            $this->successRespnose['message'] = "login successful"; 
		            $this->successRespnose['token'] = $token; 
		            $this->responseData($this->successRespnose);
		            // exit;
		        }
		        else {
		            $this->errorResponse['message'] = 'Invalid username or password!';
					$this->responseData($this->errorResponse);
					// exit;
		        }
			} else {
				$this->errorResponse['message'] = str_replace("\n", '', strip_tags(validation_errors()));
				$this->responseData($this->errorResponse);
				// exit;
			}
		} else {
			$this->errorResponse['message'] = 'Invalid Request';
			$this->responseData($this->errorResponse);
			// exit;
		}
	}


	public function testjwt() {

		$tokenData = array('username' => 'test');
		$token = AUTHORIZATION::generateToken($tokenData);
		echo $token;
	}


	private function responseData($responsedata, $sendToken = true) {
		$this->output->set_content_type('application/json');//set_output(json_encode($responsedata));
		echo json_encode($responsedata);
		exit;
	}

	private function verify_request()
	{
	    // Get all the headers
	    $headers = $this->input->request_headers();
	    // Extract the token
	    $token = $headers['Authorization'];
	    // Use try-catch
	    // JWT library throws exception if the token is not valid
	    try {
	        // Validate the token
	        // Successfull validation will return the decoded user data else returns false
	        $data = AUTHORIZATION::validateToken($token);
	        if ($data === false) {
	        	$this->errorResponse['message'] = 'Unauthorized Access!';
				$this->responseData($this->errorResponse);
	            exit();
	        } else {
	            return $data;
	        }
	    } catch (Exception $e) {
	    	
	        // Token is invalid
	        // Send the unathorized access message
	        $this->errorResponse['message'] = 'Unauthorized Access!';
			$this->responseData($this->errorResponse);
			exit;
	    }
	}

	

}
