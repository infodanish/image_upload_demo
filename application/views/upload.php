<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap Simple Login Form</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="<?php echo base_url('/public/js/jquery-3.3.1.min.js'); ?>" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 



<script src="<?php echo base_url('/public/js/form/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('/public/js/validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('/public/js/blockui/jquery.blockUI.js'); ?>"></script>
<script src="<?php echo base_url('/public/js/alphanum/jquery.alphanum.js'); ?>"></script>
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
    input[type='text'].error, input[type='password'].error, textarea.error,  select.error{ 
        border: 1px solid #B94A48 !important;
    }
    label.error {
        color:#B94A48;
        display:none;    
    }
</style>
<script>
    
  $(document).ready(function() {


    $(".numberonly").numeric('positiveInteger');
      

      $("#frm_login").validate({    
          errorPlacement: function(error,element) { return true; },
          submitHandler: function(form) {
              $("#frm_login").ajaxSubmit({target: '#response', beforeSubmit: function (formData, jqForm, options) {
                    $.blockUI({message:  ''});
                  }, clearForm: false, dataType: 'json', success: function (resObj, statusText) {
                      $.unblockUI();
                      if (resObj.status == "success") {                               
                          $('.alert').hide();
                          $('.success_message').html(resObj.message).parent().show();
                          $(".img_url").html("URL: " + resObj.url); 
                          $(".img_src").attr('src', resObj.url);
                          $("#frm_login").clearForm();
                      } else {    
                          //
                          $('.alert').hide();
                          $('.error_message').html(resObj.message).parent().show();
                          setTimeout(function(){ $('.error_message').parent().hide();}, 5000);
                      }
                  }
              });
          }
      });
  });
  </script>
</head>
<body>
    
<div class="login-form">
    
    <?php echo form_open_multipart(site_url('demo/doupload'), array('id' => 'frm_login', 'class'=> '', 'role'=>'form', 'autocomplete' => "off")) ?>
        <p class="text-right"><a href="<?php echo site_url('demo/logout'); ?>">Logout</a></p>
        <h2 class="text-center">Demo Upload</h2>   
        <div class="form-group">
            <input type="file" id="image_file" name="image_file" class="form-control" placeholder="Upload Image" />
        </div>
        <div class="form-group">
            <input type="url" id="image_url" name="image_url" class="form-control" placeholder="Image URL" />
        </div>
        <div class="form-group">
            <input type="text" id="width" name="width" class="form-control numberonly" placeholder="Width" required="required" />
        </div>
        <div class="form-group">
            <input type="text" id="height" name="height" class="form-control numberonly" placeholder="Height" required="required" />
        </div>    
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Upload</button>
        </div>

        <div class="alert alert-danger alert_position" style="display:none;" >
            <p class="error_message"></p>
        </div>
        <div class="alert alert-success alert_position" style="display:none;">
            <p class="success_message"></p>
            <p class="img_url"></p>
            <img class="img_src" src="" />
        </div>
               
    </form>
    
</div>
</body>
</html>                                		                            